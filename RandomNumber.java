package bcas.ap.array;

import java.util.Random;

public class RandomNumber {
	public static void createRanArray(int size, int range) {

		Random ran = new Random();
		int[] randArray = new int[size];

		for (int r = 0; r < size; r++) {
			randArray[r] = ran.nextInt(range);

		}

		printRandomArray(randArray);
	}

	private static void printRandomArray(int[] array) {
		for (int element : array) {
			System.out.print(element + ",");

		}
	}
}
